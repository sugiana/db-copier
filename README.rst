Database Copier
===============

Copy database structure and data use SQLAlchemy.

Copy ``batchmove.ini`` to ``my.ini`` and change as necessary then run::

  $ python batchmove.py my.ini

You also can show table structure in SQL style::

  $ python show_structure.py my.ini --table=employee

or in SqlAlchemy style::

  $ python show_structure.py my.ini --table=employee --sqla

Happy migration.

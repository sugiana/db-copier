from sqlalchemy import (
    engine_from_config,
    MetaData,
    Table,
    select,
    func,
    inspect,
    )
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


Base = declarative_base()


class BaseModel:
    def to_dict(self):
        values = {}
        for column in self.__table__.columns:
            values[column.name] = getattr(self, column.name)
        return values

    def from_dict(self, values):
        for column in self.__table__.columns:
            if column.name in values:
                setattr(self, column.name, values[column.name])


class DBProfile:
    def __init__(self, db_conf: dict, prefix_conf: str):
        self.engine = engine_from_config(db_conf, prefix_conf)
        self.inspect = inspect(self.engine)
        self.metadata = MetaData()
        base_session = sessionmaker(bind=self.engine)
        self.session = base_session()

    def get_schema_names(self):
        return self.inspect.get_schema_names()

    def get_table_names(self, schema=None):
        return self.inspect.get_table_names(schema)

    def query(self, *args):
        return self.session.query(*args)

    def flush(self, row=None):
        row and self.session.add(row)
        self.session.flush()

    def commit(self, row=None):
        row and self.flush(row)
        self.session.commit()

    def execute(self, sql, **kwargs):
        with self.engine.connect() as conn:
            return conn.execute(sql, **kwargs)

    def first_value(self, sql):
        q = self.engine.execute(sql)
        row = q.first()
        return row[0]

    def get_table(self, tablename, schema=None):
        return Table(
            tablename, self.metadata, autoload_with=self.engine, schema=schema)

    def get_orm(self, tablename, schema=None):
        class T(Base, BaseModel):
            __table__ = self.get_table(tablename, schema)
        return T

    def get_count(self, table):
        sql = select(func.count()).select_from(table)
        q = self.execute(sql)
        return q.scalar()

import os
import sys
import locale
import logging
from copy import copy
from string import printable
from decimal import Decimal
from argparse import ArgumentParser
from configparser import RawConfigParser
from time import time
from datetime import datetime
from sqlalchemy import (
    Column,
    Table,
    String,
    Text,
    Integer,
    BigInteger,
    Float,
    DateTime,
    Boolean,
    LargeBinary,
    func,
    select,
    exists,
    )
from sqlalchemy.exc import IntegrityError
from sqlalchemy.schema import ForeignKeyConstraint
from sqlalchemy.sql.expression import text
from models import (
    DBProfile,
    Base,
    BaseModel,
    )
from tools import (
    module_source,
    is_same,
    plain_value,
    humanize_time,
    Progress,
    thousand,
    get_primary_key,
    get_first_fields,
    )
from logger import setup_logging


BooleanTypes = ('bit',)
FloatTypes = ('money', Decimal)


def get_target_name(source_name):
    if lowercase_target:
        return source_name.lower()
    return source_name


def clean_value(v):
    for line in conf.get('main', 'replace_chars').strip().splitlines():
        a, b = line.split(',')
        a = bytes.fromhex(a)
        b = bytes.fromhex(b)
        a = chr(ord(a))
        b = chr(ord(b))
        v = v.replace(a, b)
    if not conf.getboolean('main', 'printable_chars_only'):
        return v
    r = ''
    for ch in v:
        if ch in printable:
            r += ch
        else:
            r += ' '
    return r


def get_dict(source):
    dict_source = dict(source._mapping)
    dict_target = dict()
    for key in dict_source:
        fname = get_target_name(key)
        value = dict_source[key]
        if isinstance(value, str):
            value = clean_value(value)
        dict_target[fname] = value
    return dict_target


def to_dict(t):
    i = -1
    r = dict()
    for fname in t.keys():
        i += 1
        r[fname] = t[i]
    return r


def get_target_columns(source_columns):
    target_columns = []
    for name in source_columns:
        fname = get_target_name(name)
        target_columns.append(fname)
    return target_columns


def change_column_def(c):
    fieldname = get_target_name(c.name)
    orig_type = str(c.type).lower()
    if orig_type in BooleanTypes:
        return Column(
                Boolean, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if c.type.python_type is float and hasattr(c.type, 'precision'):
        if c.type.precision > 10:
            return Column(
                    Float, nullable=c.nullable, name=fieldname,
                    primary_key=c.primary_key)
        return Column(
                Integer, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if orig_type in FloatTypes or c.type.python_type in FloatTypes:
        return Column(
                Float, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if c.type.python_type is str:
        if c.type.length:
            typ = String(c.type.length)
        else:
            typ = Text
        return Column(
                typ, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if c.type.python_type is int:
        if orig_type == 'bigint':
            return Column(
                    BigInteger, nullable=c.nullable, name=fieldname,
                    primary_key=c.primary_key)
        return Column(
                Integer, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if hasattr(c.server_default, 'arg') \
            and hasattr(c.server_default.arg, 'text') \
            and c.server_default.arg.text.strip().lower() == 'sysdate':
        return Column(
                DateTime, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key, default=func.now)
    if c.type.python_type is datetime:
        return Column(
                DateTime, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    if c.type.python_type is bytes:
        return Column(
                LargeBinary, nullable=c.nullable, name=fieldname,
                primary_key=c.primary_key)
    return Column(
            c.type, nullable=c.nullable, name=fieldname,
            primary_key=c.primary_key)


def has_primary_key(t):
    for c in t.columns:
        if c.primary_key:
            return True


def load_plugin(filename):
    module_name = os.path.split(filename)[-1].split('.')[0]
    return module_source(module_name, filename)


# Get schema in tablename
def split_tablename(tablename):
    t = tablename.split('.')
    if t[1:]:
        return t[0], t[1]
    return None, tablename


def log_debug(msg):
    if option.debug:
        log.debug(msg)


def validation_when_no_pkeys():
    if copy_option in ('update', 'sync', 'sync_and_delete'):
        raise Exception('Cannot update without primary keys')


class SqlGenerator:
    def __init__(self, source_table, target_table, pkeys, method='sync'):
        self.source_table = source_table
        self.target_table = target_table
        self.pkeys = pkeys
        functions = dict(
            insert=self.insert,
            update=self.update,
            sync=self.insert_update,
            sync_and_delete=self.insert_update)
        self.func = functions[method]
        self.last_log = []

    def where_pkeys(self, sql, data, table):
        for field in self.pkeys:
            f = getattr(table.c, field)
            v = data[field]
            sql = sql.where(f == v)
        return sql

    def get_row_by_query(self, data, table):
        sql = select([table])
        sql = self.where_pkeys(sql, data, table)
        q = table.metadata.bind.execute(sql)
        return q.fetchone()

    def log_info(self, topic, data):
        msg = '{} {} {}'.format(self.target_table.name, topic, data)
        log.info(msg)

    def log_warning(self, topic, data):
        msg = '{} {} {}'.format(self.target_table.name, topic, data)
        log.warning(msg)

    def get_sql(self, data):
        return self.func(data)

    def insert_update(self, data):
        target_row = self.get_row_by_query(data, self.target_table)
        if target_row:
            return self.update_row(target_row, data)
        return self.insert(data)

    def insert(self, data):
        self.log_info('INSERT', data)
        return self.target_table.insert().values(data)

    def update(self, data):
        target_row = self.get_row_by_query(data, self.target_table)
        if target_row:
            return self.update_row(target_row, data)
        self.log_warning('not found', data)

    def update_row(self, target_row, data):
        vals = self.need_update(data, target_row)
        if not vals:
            self.log_warning('already same', data)
            return
        self.log_info('UPDATE', ', '.join(self.last_log))
        sql = self.target_table.update()
        sql = self.where_pkeys(sql, data, self.target_table)
        return sql.values(vals)

    def need_update(self, source_dict, target_dict):
        target_update = dict()
        self.last_log = []
        for field in source_dict:
            if field not in target_dict:
                continue
            source_value = source_dict[field]
            target_value = target_dict[field]
            if is_same(source_value, target_value):
                continue
            target_update[field] = source_value
            log_source_value = plain_value(source_value)
            log_target_value = plain_value(target_value)
            self.last_log.append(
                '{f} {t} to be {s}'.format(
                    f=field, t=[log_target_value], s=[log_source_value]))
        return target_update


def load_event(plugin, event_name):
    try:
        return getattr(plugin, event_name)
    except AttributeError:
        pass


class BatchMove:
    def __init__(
            self, db_source, db_target, schema_source=None,
            schema_target=None):
        self.db_source = db_source
        self.db_target = db_target
        self.schema_source = schema_source
        self.schema_target = schema_target
        self.plugin_names = []
        self.plugins = []

    def copy_structure(self, Source):
        cols = []
        is_pkey = has_primary_key(Source)
        if not is_pkey:
            seq_name = f'{Source.name}_id_seq'
            cols.append(Column('id', Integer, primary_key=True).copy())
        for c in Source.columns:
            c = change_column_def(c)
            cols.append(c)
        constraints = []
        copy_constraints = copy(Source.constraints)
        for c in copy_constraints:
            if c.__class__ == ForeignKeyConstraint:
                c = self.change_foreign_key(c)
                constraints.append(c)
        tablename_target = get_target_name(Source.name)
        Target = Table(
            tablename_target, self.db_target.metadata,
            schema=self.schema_target, *(cols + constraints))
        log.info(f'Create table {Source.name}')
        Target.create(bind=self.db_target.engine)

    def get_target_table(self, Source):
        name = get_target_name(Source.name)
        return self.db_target.get_table(name, self.schema_target)

    def copy_data(self, Source, Temp_Pkeys=None):
        count = self.db_source.get_count(Source)
        log.info(f'Count {Source.name} {count} record')
        order_by = pkeys = get_primary_key(Source)
        if not pkeys:
            validation_when_no_pkeys()
            order_by = get_first_fields(Source)
        order_by = [f'"{x}"' for x in order_by]
        order_by = text(', '.join(order_by))
        sql_source = Source.select().order_by(order_by)
        Target = self.get_target_table(Source)
        sync = SqlGenerator(Source, Target, pkeys, copy_option)
        if copy_option == 'insert':
            offset = self.db_target.get_count(Target)
        else:
            offset = 0
        found = True
        pr = Progress(count, offset)
        while found:
            found = False
            sql = sql_source.offset(pr.offset).limit(limit)
            sources = self.db_source.execute(sql)
            for source in sources:
                found = True
                data = get_dict(source)
                sql = sync.get_sql(data)
                if sql is not None:
                    self.execute(sql)
                if copy_option == 'sync_and_delete':
                    d = dict()
                    for key in pkeys:
                        d[key] = data[key]
                    sql = Temp_Pkeys.insert().values(d)
                    self.execute(sql)
                estimate = pr.get_estimate()
                if estimate:
                    e = humanize_time(estimate)
                    log.info(
                        f'{Source.name} #{pr.offset}/{count} '
                        f'{pr.speed} second/row estimate {e}')
        return Target

    def copy(self, tablename):
        target_tablenames = self.db_target.get_table_names(
                self.schema_target)
        Source = self.db_source.get_table(tablename, self.schema_source)
        if get_target_name(tablename) not in target_tablenames:
            self.copy_structure(Source)
        if copy_option == 'sync_and_delete':
            Temp = self.create_temporary_pkeys(Source)
        else:
            Temp = None
        Target = self.copy_data(Source, Temp)
        if Temp is None:
            return
        # Delete where not exists
        sql_subq = select([1]).select_from(Temp)
        for temp_f in Temp.columns:
            f = getattr(Target.c, temp_f.name)
            sql_subq = sql_subq.where(temp_f == f)
        sql = select([Target]).where(~exists(sql_subq))
        for row in self.db_target.engine.execute(sql):
            sql = Target.delete()
            for temp_f in Temp.columns:
                f = getattr(Target.c, temp_f.name)
                v = getattr(row, temp_f.name)
                sql = sql.where(f == v)
            self.db_target.engine.execute(sql)
            d = dict(row)
            log.info(f'{Target.name} delete {d}')
        Temp.drop(self.db_target.engine)

    def change_foreign_key(self, constraint):
        references = []
        for e in constraint.elements:
            t = e.target_fullname.split('.')
            ref_fieldname = t[-1]
            ref_tablename = t[-2]
            ref = [ref_tablename, ref_fieldname]
            if self.schema_target:
                ref = [self.schema_target] + ref
            ref = '.'.join(ref)
            ref = get_target_name(ref)
            references.append(ref)
        column_names = [column.name for column in constraint.columns]
        column_names = get_target_columns(column_names)
        return ForeignKeyConstraint(column_names, references)

    def _get_target_fkeys(self, table):
        fkeys = dict()
        for c in table.constraints:
            if c.__class__ != ForeignKeyConstraint:
                continue
            refs = [e.target_fullname for e in c.elements]
            t = refs[0].split('.')[:-1]
            ref_table = t[-1]
            ref_schema = t[1:] and t[0] or None
            TRef = self.db_target.get_table(ref_table, ref_schema)
            ref_columns = [column.split('.')[-1] for column in refs]
            ref_pkeys = get_primary_key(TRef)
            ref_table_full = '.'.join(t)
            fkeys[ref_table_full] = (c.columns, ref_columns, TRef,
                                     ref_pkeys)
            log_debug(
                f' fkey {ref_table_full} {c.columns} {ref_columns}, '
                f'pkeys {ref_pkeys}')
        return fkeys

    def _fkey_data(self, fkeys, data):
        for fkey in foreign_keys:
            vals = []
            where = dict()
            fnum = -1
            for fname in fkey['sql_fields']:
                fnum += 1
                val = data[fname]
                vals.append(val)
                ref_fname = fkey['ref_fields'][fnum]
                where[ref_fname] = val
            ref_table = fkey['ref_table']
            columns, ref_columns, TRef, pkeys = fkeys[ref_table]
            log_debug(f'source {ref_table} where {where}')
            q = self.db_target.query(TRef).filter_by(**where)
            row_ref = q.first()
            if not row_ref:
                raise Exception(
                    'foreign values not found: table {t} {w}'.format(
                        t=ref_table, w=where))
            row_ref = to_dict(row_ref)
            for pkey in pkeys:
                val = row_ref[pkey]
                log_debug(f'pkey val {val}')
            fnum = -1
            for column in columns:
                fnum += 1
                pkey = ref_columns[fnum]
                pkey_val = row_ref[pkey]
                data[column] = pkey_val
            log_debug(f'data {d}')
            for fname in fkey['sql_fields']:
                del data[fname]
        return data

    def load_plugin(self, name):
        if name in self.plugin_names:
            i = self.plugin_names.index(name)
            plugin = self.plugins[i]
        else:
            plugin = load_plugin(name)
            self.plugin_names.append(name)
            self.plugins.append(plugin)
            try:
                setup = getattr(plugin, 'setup')
            except AttributeError:
                setup = None
            setup and setup(self)
        return plugin

    def __insert(self, source, target):
        try:
            self.db_target.flush(target)
        except IntegrityError as e:
            self.duplicate_key_error(e)
        self.db_target.commit()

    def copy_data_from_query(self, tablename, plugin, foreign_keys=[]):
        schema_target, tablename_target = split_tablename(tablename)
        table = self.db_target.get_table(tablename_target, schema_target)
        fkeys = foreign_keys and self._get_target_fkeys(table)

        class Target(Base, BaseModel):
            __table__ = table

        before_save = load_event(plugin, 'before_save')
        qry = plugin.Query(self.db_source, tablename)
        count = qry.get_count()
        if not count:
            log.info('0 rows.')
            return
        s_count = thousand(count)
        sql = qry.get_select()
        sql = sql.limit(1)
        last_estimate = None
        found = True
        pr = Progress(count)
        while found:
            found = False
            sql = sql.offset(pr.offset)
            q = self.db_source.execute(sql)
            for source in q:
                found = True
                data = get_dict(source)
                if foreign_keys:
                    data = self._fkey_data(fkeys, data)
                target = Target()
                target.from_dict(data)
                before_save and before_save(self, source, target)
                self.__insert(source, target)
                estimate = pr.get_estimate()
                if not estimate:
                    continue
                e = humanize_time(estimate)
                no = thousand(pr.offset)
                log.info(f'{tablename} #{no}/{s_count} estimate {e}')

    def copy_data_from_sql(self, tablename, sql, plugin=None):
        log.info(f'{sql} -> {tablename}')
        schema_target, tablename_target = split_tablename(tablename)
        table = self.db_target.get_table(tablename_target, schema_target)
        pkeys = get_primary_key(table)
        sources = self.db_source.execute(sql.strip())
        for source in sources:
            data = get_dict(source)
            sync = SqlGenerator(None, table, pkeys, copy_option)
            sql = sync.get_sql(data)
            if sql is not None:
                self.execute(sql)

    def execute(self, sql):
        try:
            self.db_target.execute(sql)
        except IntegrityError as e:
            self.duplicate_key_error(e)

    def duplicate_key_error(self, err):
        err_ = str(err).lower()
        found = False
        for ref_err in duplicate_key_errors:
            if err_.find(ref_err) > -1:
                self.db_target.session.rollback()
                return
        raise err

    def create_temporary_pkeys(self, Source):
        tablename_target = '_' + get_target_name(Source.name)
        log.info(f'Create temporary primary keys {tablename_target}')
        cols = []
        for c in Source.columns:
            if c.primary_key:
                c = change_column_def(c)
                cols.append(c.copy())
        Target = Table(
                tablename_target, self.db_target.metadata,
                schema=self.schema_target, *(cols))
        Target.create()
        return Target


help_file = 'configuration file'

pars = ArgumentParser()
pars.add_argument('conf', help=help_file)
pars.add_argument('--debug', action='store_true')
option = pars.parse_args(sys.argv[1:])

conf_file = option.conf
setup_logging(conf_file)
log = logging.getLogger(__name__)

conf = RawConfigParser()
conf.read(conf_file)
conf['DEFAULT'] = dict(
    schema_source='',
    schema_target='',
    lowercase_target='true',
    replace_chars='',
    printable_chars_only='true',
    copy_option='insert',
    limit='100',
    duplicate_key_errors='duplicate key\nnilai kunci ganda')

locale_format = conf.get('main', 'locale_format')
if locale_format:
    locale.setlocale(locale.LC_ALL, locale_format)

cf = dict(conf.items('main'))
db_source = DBProfile(cf, 'source.')
db_target = DBProfile(cf, 'target.')

schema_source = conf.get('main', 'schema_source') or None
schema_target = conf.get('main', 'schema_target') or None

copy_option = conf.get('main', 'copy_option')
tablenames = conf.get('main', 'copy_tablenames')
tablenames = tablenames.strip().splitlines()

lowercase_target = conf.getboolean('main', 'lowercase_target')
duplicate_key_errors = conf.get('main', 'duplicate_key_errors')
duplicate_key_errors = duplicate_key_errors.strip().splitlines()

limit = conf.getint('main', 'limit')

bmove = BatchMove(db_source, db_target, schema_source, schema_target)

for tokens in tablenames:
    t = tokens.split()
    tablename = t[0]
    script_file = t[1:] and t[1]
    foreign_keys = []
    if script_file:
        ext = os.path.splitext(script_file)[1]
        if t[2:]:
            fkeys_str = t[2]
            for line in fkeys_str.split(';'):
                fkey, ref = line.split(',')
                foreign_keys.append((fkey, ref))
        sql = None
        if ext == '.py':
            plugin = bmove.load_plugin(script_file)
            get_sql = load_event(plugin, 'get_sql')
            if get_sql:
                sql = get_sql()
                bmove.copy_data_from_sql(tablename, sql, plugin)
            else:
                bmove.copy_data_from_query(tablename, plugin, foreign_keys)
        else:
            with open(script_file) as f:
                sql = f.read()
            bmove.copy_data_from_sql(tablename, sql)
    else:
        bmove.copy(tablename)

import os
import sys
import csv
from datetime import datetime
from argparse import ArgumentParser
from configparser import ConfigParser
from sqlalchemy import PrimaryKeyConstraint
from models import (
    DBProfile,
    Base,
    BaseModel,
    )
from tools import (
    datetime_from_str,
    get_primary_key,
    )


# Get schema in tablename
def split_tablename(tablename):
    t = tablename.split('.')
    if t[1:]:
        return t[0], t[1]
    return None, tablename


def get_pkeys(table):
    r = []
    for c in table.constraints:
        if c.__class__ is PrimaryKeyConstraint:
            for col in c:
                r.append(col.name)
            return r
    return r


def do_nothing(s):
    return s


def string_handler(c):
    if c.type.python_type is datetime:
        return datetime_from_str
    return do_nothing


pars = ArgumentParser()
pars.add_argument('conf', help='Configuration file')
pars.add_argument('--input', help='CSV file')
pars.add_argument('--table', help='restore to table')
pars.add_argument('--unique')
option = pars.parse_args(sys.argv[1:])

conf_file = option.conf
conf = ConfigParser()
conf.read(conf_file)
cf = dict(conf.items('main'))

filename = option.input
schema, tablename = split_tablename(option.table)

db = DBProfile(cf, 'target.')
target_table = db.get_table(tablename, schema)
target_fields = []
string_handlers = dict()
for c in target_table.columns:
    target_fields.append(c.name)
    string_handlers[c.name] = string_handler(c)


class Target(Base, BaseModel):
    __table__ = target_table


source = None
with open(filename) as f:
    reader = csv.DictReader(f)
    for source in reader:
        break
if not source:
    sys.exit()

source2target_fields = dict()
target2source_fields = dict()
for source_field in source:
    if source_field in target_fields:
        target_field = source_field
    elif source_field.lower() in target_fields:
        target_field = source_field.lower()
    elif source_field.upper() in target_fields:
        target_field = source_field.upper()
    else:
        target_field = None
    if target_field:
        source2target_fields[source_field] = target_field
        target2source_fields[target_field] = source_field
        print('{s} -> {t}'.format(s=source_field, t=target_field))
    else:
        print('{s} does not exists on {t}'.format(
            s=source_field, t=option.table))

if option.unique:
    if option.unique == 'auto':
        target_keys = get_pkeys(target_table)
    else:
        target_keys = option.unique.split(',')
with open(filename) as f:
    reader = csv.DictReader(f)
    for source in reader:
        if option.unique:
            filter_ = {}
            for target_key in target_keys:
                source_key = target2source_fields[target_key]
                val = source[source_key] or None
                filter_[target_key] = val
            q = db.query(Target).filter_by(**filter_)
            if q.first():
                continue
        target_data = dict()
        for source_field in source:
            if source_field not in source2target_fields:
                continue
            val = source[source_field] or None
            func = string_handlers[source_field]
            val = func(val)
            target_field = source2target_fields[source_field]
            target_data[target_field] = val
        target = Target()
        target.from_dict(target_data)
        db.commit(target)

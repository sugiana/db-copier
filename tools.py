import sys
import re
from datetime import datetime
from urllib.parse import unquote_plus
from urllib.parse import urlparse
from decimal import Decimal
from time import time
from datetime import (
    date,
    datetime,
    time as mktime,
    )
import re
import sys
import os
import pytz
import locale
import importlib.machinery


#######
# Log #
#######
def humanize_time(secs):
    mins, secs = divmod(secs, 60)
    hours, mins = divmod(mins, 60)
    return '%02d:%02d:%02d' % (hours, mins, secs)


def plain_value(v):
    if isinstance(v, str):
        return v
    if isinstance(v, datetime):
        return time_str(v)
    if isinstance(v, date):
        return date_str(v)
    if isinstance(v, Decimal):
        return float(v)
    return v


def plain_values(d):
    r = dict()
    for key in d:
        v = d[key]
        r[str(key)] = plain_value(v)
    return r


def time_str(v):
    return '{y}-{m}-{d} {hh}:{mm}:{ss}'.format(
        d=v.day, m=v.month, y=v.year, hh=v.hour, mm=v.minute, ss=v.second)


class Progress:
    def __init__(self, count, offset=0):
        self.begin_time = time()
        self.last_estimate = None
        self.count = count
        self.offset = offset
        self.current_count = 0

    def get_estimate(self):
        self.current_count += 1
        self.offset += 1
        remain = self.count - self.offset
        duration = time() - self.begin_time
        self.speed = duration / self.current_count
        estimate = int(self.speed * remain)
        if estimate == self.last_estimate:
            return
        self.last_estimate = estimate
        return estimate


############
# Database #
############
# Need for MS SQL Server
def vs(s):
    return s and unicode(s).encode('utf-8') or None


def is_odbc(driver):
    return driver.split('+')[-1] == 'pyodbc'


def extract_netloc(s):  # sugiana:a@localhost:5432
    r = {}
    t = s.split('@')
    if t[1:]:  # localhost:5432
        h = t[1].split(':')
        if h[1:]:
            r['port'] = int(h[1])
        r['host'] = h[0]
    auth = t[0].split(':')
    if auth[1:]:
        r['pass'] = auth[1]
    r['user'] = auth[0]
    return r


def extract_tds(s):
    items = s.split(';')
    r = {}
    for item in items:
        key, value = item.split('=')
        if key == 'UID':
            r['user'] = value
        elif key == 'PWD':
            r['pass'] = value
        elif key == 'Server':
            r['host'] = value
        elif key == 'Database':
            r['name'] = value
        elif key == 'Port':
            r['port'] = int(value)
    return r


def extract_db_url(db_url):
    p = urlparse(db_url)
    r = {'driver': p.scheme}
    if is_odbc(p.scheme):
        if p.query:
            s = p.query
        else:
            s = p.path.split('?')[-1]
        t = s.split('=')
        s = unquote_plus(t[1])
        r.update(extract_tds(s))
        return r
    if p.netloc:
        r.update(extract_netloc(p.netloc))
    if p.path[1:]:
        r['name'] = p.path.lstrip('/')
    return r


def eng_profile(db_url):
    url = extract_db_url(db_url)
    return 'driver:%s user:%s host:%s port:%s database:%s' % (
        url['driver'], url['user'], url['host'], url['port'], url['name'])


def trigger_name(sql):
    sql = sql.lower().replace('\n', ' ')
    match = re.compile('trigger (.*) (after|before)').search(sql)
    return match and match.group(1)


def split_schema(table_name):
    t = table_name.split('.')
    if t[1:]:
        return t
    return None, table_name


def sql_count(sql):
    sql = one_space(sql)
    sql = sql.lower()
    regex = re.compile('select (.*) from (.*) order by')
    match = regex.search(sql)
    if match:
        from_s = match.group(2)
        return f'SELECT count(*) FROM {from_s}'
    regex = re.compile('select (.*) from (.*)')
    match = regex.search(sql)
    if match:
        from_s = match.group(2)
        return f'SELECT count(*) FROM {from_s}'


def get_primary_key(t):
    k = []
    for c in t.columns:
        if c.primary_key:
            k.append(c.name)
    return k


def get_first_fields(t, field_count=10):
    fields = []
    i = 0
    for c in t.columns:
        i += 1
        fields.append(c.name)
        if i > field_count:
            break
    return fields


##########
# String #
##########
def clean(s):
    r = ''
    for ch in s:
        ascii = ord(ch)
        if ascii > 126 or ascii < 32:
            ch = ' '
        r += ch
    return r


def to_str(s):
    if s is None:
        s = ''
    elif not isinstance(s, str):
        s = str(s)
    return clean(s)


def left(s, width):
    s = to_str(s)
    return s.ljust(width)[:width]


def right(s, width):
    s = to_str(s)
    return s.zfill(width)[:width]


def one_space(s):
    while s.find('  ') > -1:
        s = s.replace('  ', ' ')
    return s


##################
# Data Structure #
##################
class FixLength:
    def __init__(self, struct):
        self.set_struct(struct)

    def set_struct(self, struct):
        self.struct = struct
        self.fields = {}
        new_struct = []
        for s in struct:
            name = s[0]
            size = s[1:] and s[1] or 1
            typ = s[2:] and s[2] or 'A'  # N: numeric, A: alphanumeric
            self.fields[name] = {'value': None, 'type': typ, 'size': size}
            new_struct.append((name, size, typ))
        self.struct = new_struct

    def set(self, name, value):
        self.fields[name]['value'] = value

    def get(self, name):
        return self.fields[name]['value']

    def __setitem__(self, name, value):
        self.set(name, value)

    def __getitem__(self, name):
        return self.get(name)

    def get_raw(self):
        s = ''
        for name, size, typ in self.struct:
            v = self.fields[name]['value']
            pad_func = typ == 'N' and right or left
            if typ == 'N':
                v = v or 0
                i = int(v)
                if v == i:
                    v = i
            else:
                v = v or ''
            s += pad_func(v, size)
        return s

    def set_raw(self, raw):
        awal = 0
        for t in self.struct:
            name = t[0]
            size = t[1:] and t[1] or 1
            akhir = awal + size
            value = raw[awal:akhir]
            if not value:
                return
            self.set(name, value)
            awal += size
        return True

    def from_dict(self, d):
        for name in d:
            value = d[name]
            self.set(name, value)


###########
# Numeric #
###########
def should_int(value):
    int_ = int(value)
    return int_ == value and int_ or value


def thousand(value, float_count=None):
    if float_count is None:  # autodetection
        if isinstance(value, int):
            float_count = 0
        else:
            float_count = 2
    return locale.format_string('%%.%df' % float_count, value, True)


def money(value, float_count=None, currency=None):
    if value < 0:
        v = abs(value)
        format_ = '(%s)'
    else:
        v = value
        format_ = '%s'
    if currency is None:
        currency = locale.localeconv()['currency_symbol']
    s = ' '.join([currency, thousand(v, float_count)])
    return format_ % s


############
# Datetime #
############
timezone_name = 'Asia/Jakarta'
if sys.platform.find('linux') > -1:
    timezone_file = '/etc/timezone'
    if os.path.exists(timezone_file):
        timezone_name = open(timezone_file).read().strip()


def get_timezone():
    return pytz.timezone(timezone_name)


# As local timezone
def as_timezone(tz_date):
    localtz = get_timezone()
    return tz_date.astimezone(localtz)


def create_datetime(year, month, day,
                    hour=0, minute=7, second=0,
                    microsecond=0):
    tz = get_timezone()
    return datetime(year, month, day,
                    hour, minute, second,
                    microsecond, tzinfo=tz)


def create_date(year, month, day):
    return create_datetime(year, month, day)


def create_now():
    tz = get_timezone()
    return datetime.now(tz)


def date_str(v):
    return '{y}-{m}-{d}'.format(y=v.year, m=v.month, d=v.day)


def datetime_from_str(s):
    date_, time_ = s.split()
    y, m, d = [int(x) for x in date_.split('-')]
    hh, mm, ss = [int(x) for x in time_.split(':')]
    return create_datetime(y, m, d, hh, mm, ss)


##############
# Dictionary #
##############
def dict_copy(d, keys):
    r = dict()
    for key in keys:
        r[key] = d[key]
    return r


#################
# Compare value #
#################
MIDNIGHT_TIME = mktime(0, 0, 0)


def split_time(t):
    if type(t) is datetime:
        return t.date(), t.time()
    return t, MIDNIGHT_TIME


def is_same(a, b):
    if a == b:
        return True
    if not (isinstance(a, date) or isinstance(a, datetime)):
        return False
    date_a, time_a = split_time(a)
    date_b, time_b = split_time(b)
    if date_a != date_b:
        return False
    if isinstance(a, date) or isinstance(b, date):
        return True
    if time_a == time_b:
        return True
    return False


##########
# Module #
##########
def module_source(name, filename):
    if sys.version_info.major == 2:
        return imp.load_source(name, filename)
    loader = importlib.machinery.SourceFileLoader(name, filename)
    return loader.load_module()

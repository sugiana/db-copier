import sys
from daemon import stop_daemon


pid_file = sys.argv[1]
stop_daemon(pid_file)

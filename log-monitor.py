# Log file monitor, when timeout do something.

import os
import sys
import re
from datetime import datetime
from optparse import OptionParser
from subprocess import call
from daemon import (
    read_pid_file,
    is_live,
    write_pid_file,
    )


def out(msg, code=1):
    print(msg)
    sys.exit(code)


MSG_AGE_UNDER = 'The log age is still under {n} seconds, {e} does not need '\
        'to be run.'
MSG_AGE_ABOVE = 'The log age is above {d} seconds, run {e} now.'
MSG_INVALID_DATETIME = 'Invalid datetime format, run {} now.'

REGEX_TIME = re.compile(r'^(\d*)\-(\d*)-(\d*) (\d*):(\d*):(\d*)')
DEFAULT_MAX_AGE = 180  # seconds

HELP_AGE = 'default {} seconds'.format(DEFAULT_MAX_AGE)
HELP_EXECUTE = 'Execute this when timeout.'

pars = OptionParser()
pars.add_option('-l', '--log-file')
pars.add_option('-x', '--execute', help=HELP_EXECUTE)
pars.add_option('-a', '--age', default=DEFAULT_MAX_AGE, help=HELP_AGE)
pars.add_option('-p', '--pid-file')

option, remain = pars.parse_args(sys.argv[1:])

if option.pid_file:
    pid = read_pid_file(option.pid_file)
    if pid:
        if is_live(pid):
            msg = 'My PID {} still active'.format(pid)
            out(msg)
    write_pid_file(option.pid_file)

log_file = option.log_file
exe = os.path.realpath(option.execute)
max_age = int(option.age)

if not os.path.exists(exe):
    msg = 'File {} not found.'.format(exe)
    out(msg, 2)

last_match = None
f = open(log_file)
for line in f.readlines():
    s = line.strip()
    match = REGEX_TIME.search(s)
    if match:
        last_match = match
f.close()
if last_match:
    y, m, d, hh, mm, ss = last_match.groups()
    y, m, d, hh, mm, ss = int(y), int(m), int(d), int(hh), int(mm), int(ss)
    log_time = datetime(y, m, d, hh, mm, ss)
    kini = datetime.now()
    usia_log = kini - log_time
    usia_log = usia_log.seconds
    print('Log: {}'.format(log_time))
    print('Now: {}'.format(kini))
    print('Age: {} seconds'.format(usia_log))
    if usia_log < max_age:
        msg = MSG_AGE_UNDER.format(n=max_age, e=exe)
        out(msg, 3)
    msg = MSG_AGE_ABOVE.format(d=max_age, e=exe)
else:
    msg = MSG_INVALID_DATETIME.format(exe)
print(msg)
call([exe])
if option.pid_file:
    os.remove(option.pid_file)

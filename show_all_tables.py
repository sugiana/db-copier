import sys
from argparse import ArgumentParser
from configparser import ConfigParser
from models import DBProfile
from show_structure import (
    registry,
    get_structure,
    )


def main(argv):
    help_conf = 'configuration file'
    pars = ArgumentParser()
    pars.add_argument('conf', help=help_conf)
    option = pars.parse_args(argv)
    conf_file = option.conf
    registry['conf'] = conf = ConfigParser()
    conf.read(conf_file)
    registry['option'] = option
    registry['db'] = db = DBProfile(conf.get('main', 'db_url_source'))
    orders = dict()
    for schema_name in db.get_schema_names():
        for table_name in db.get_table_names(schema_name):
            table = db.get_table(table_name, schema_name)
            full_name = f'{schema_name}.{table_name}'
            _, _, _, fkey_constraints, _ = get_structure(table)
            if fkey_constraints:
                orders[full_name] = dict(index=99, foreign=[])
            else:
                orders[full_name] = dict(index=1, foreign=[])
            for _, fkey_table, _ in fkey_constraints:
                if fkey_table == full_name:
                    continue
                if fkey_table.find('.') < 0:
                    fkey_table = f'public.{fkey_table}'
                orders[full_name]['foreign'].append(fkey_table)
    for full_name in orders:
        index = orders[full_name]['index']
        for foreign in orders[full_name]['foreign']:
            orders[foreign]['index'] = index - 1
    table_list = []
    for full_name in orders:
        index = orders[full_name]['index']
        table_list.append((index, full_name))
    table_list.sort()
    for index, full_name in table_list:
        print(full_name)


if __name__ == '__main__':
    main(sys.argv[1:])

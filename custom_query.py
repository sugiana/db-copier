from sqlalchemy import (
    select,
    func,
    )
from sqlalchemy.sql.expression import text
from tools import get_first_fields


class Query:
    def __init__(self, db, tablename):
        self.db = db
        self.table = self.db.get_table(tablename)

    def get_select(self):
        q = self.table.select()
        q = self.get_where(q)
        q = self.get_order_by(q)
        return q

    def get_where(self, q):
        return q

    def get_order_by(self, q):
        fieldnames = get_first_fields(self.table)
        order_by = text(', '.join(fieldnames))
        return q.order_by(order_by)

    def get_count(self):
        q = select([func.count()])
        q = q.select_from(self.table)
        q = self.get_where(q)
        return q.scalar()


if __name__ == '__main__':
    import sys
    from configparser import ConfigParser
    from models import DBProfile
    conf_file = sys.argv[1]
    tablename = sys.argv[2]
    conf = ConfigParser()
    conf.read(conf_file)
    db_url = conf.get('main', 'db_url_source')
    db = DBProfile(db_url)
    qry = Query(db, tablename)
    count = qry.get_count()
    print(count)
    sql = qry.get_select()
    offset = 0
    found = True
    while found:
        sql = sql.offset(offset).limit(1)
        offset += 1
        q = db.execute(sql)
        found = False
        for row in q:
            found = True
            print(row)

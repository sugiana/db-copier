# http://stackoverflow.com/questions/9593610/creating-a-temporary-table-from-a-query-using-sqlalchemy-orm
# http://stackoverflow.com/questions/6745189/how-do-i-get-the-name-of-an-sqlalchemy-objects-primary-key
import csv
import sys
from argparse import ArgumentParser
from configparser import (
    RawConfigParser,
    NoOptionError,
    )
from datetime import (
    datetime,
    date,
    time,
)
from decimal import Decimal
from io import StringIO

from sqlalchemy import (
    Column,
    String,
    Text,
    Integer,
    Float,
    DateTime,
    func,
    Boolean,
)
import sqlalchemy.schema
from tools import split_schema
from models import DBProfile


registry = dict()
StringTypes = ['inet', str]
IntTypes = [int]
FloatTypes = ['money', 'float', float, Decimal]
BooleanTypes = ['bit']


def get_conf(name, default=None):
    conf = registry['conf']
    try:
        return conf.get('main', name)
    except NoOptionError:
        return default


def get_target_name(source_name):
    s = str(source_name)
    t = s.split('.')
    s = t[-1]
    if get_conf('lowercase_target', 'false') == 'true':
        return s.lower()
    return s


def sql_column(c, is_single_pkey, fkey=None, unique=False):
    s_type = str(c.type).lower()
    if s_type == 'datetime':
        s_type = 'timestamp'
        if c.type.timezone:
            s_type += ' with time zone'
    elif s_type == 'time without time zone':
        s_type = 'time'
    attrs = [s_type]
    if not c.nullable:
        attrs.append('NOT NULL')
    if fkey:
        attrs.append(fkey)
    if c.primary_key and is_single_pkey:
        attrs.append('PRIMARY KEY')
    if unique:
        attrs.append('UNIQUE')
    return '{name} {attrs}'.format(
        name=c.name, type=str(c.type).lower(), attrs=' '.join(attrs))


ERR_TYPE = 'Field {fname} unknown data type {orig} or {ptype}'


def sqla_type(c):
    orig_type = str(c.type).lower()
    if orig_type in BooleanTypes:
        return 'Boolean'
    if orig_type in FloatTypes or c.type.python_type in FloatTypes:
        return 'Float'
    if c.type.python_type in StringTypes:
        if c.type.length:
            return 'String({})'.format(c.type.length)
        return 'Text'
    if c.type.python_type in IntTypes:
        return 'Integer'
    if c.type.python_type is datetime:
        if c.type.timezone:
            return 'DateTime(timezone=True)'
        return 'DateTime'
    if c.type.python_type is date:
        return 'Date'
    if c.type.python_type is bool:
        return 'Boolean'
    if c.type.python_type is time:
        return 'Time'
    if c.type.python_type is bytes:
        return 'LargeBinary'
    msg = ERR_TYPE.format(
        orig=orig_type, ptype=c.type.python_type, fname=c.name)
    raise Exception(msg)


def odoo_type(c):
    orig_type = str(c.type).lower()
    if orig_type in BooleanTypes:
        return 'Boolean'
    if orig_type in FloatTypes or c.type.python_type in FloatTypes:
        return 'Float'
    if c.type.python_type in StringTypes:
        return 'Char'
    if c.type.python_type in IntTypes:
        return 'Integer'
    if c.type.python_type is datetime:
        return 'Datetime'
    if c.type.python_type is date:
        return 'Date'
    if c.type.python_type is bool:
        return 'Boolean'
    msg = ERR_TYPE.format(
        orig=orig_type, ptype=c.type.python_type, fname=c.name)
    raise Exception(msg)


cache_sqla_import = []


def sqla_column(c, fkey=None, unique=False, mixin=False):
    fname = get_target_name(c.name)
    typ = sqla_type(c)
    imp = typ.split('(')[0]
    if imp not in cache_sqla_import:
        cache_sqla_import.append(imp)
    attrs = [typ]
    if fkey:
        attrs.append(fkey)
    if c.primary_key:
        attrs.append('primary_key=True')
    else:
        if not c.nullable:
            attrs.append('nullable=False')
        if unique:
            attrs.append('unique=True')
    if registry['option'].colander:
        title = c.name.replace('_', ' ').title()
        info = f"dict(colanderalchemy=dict(title='{title}'))"
        attrs.append(f'info={info}')
    attrs_ = ', '.join(attrs)
    if mixin:
        column_ = 'Column({})'.format(attrs_)
        return str_declared(fname, column_)
    return '{f} = Column({attrs})'.format(f=fname, attrs=attrs_)


def str_declared(func_name, return_value):
    lines = [
        '',
        ' ' * 4 + '@declared_attr',
        ' ' * 4 + 'def {}(self):'.format(func_name),
        ' ' * 8 + 'return {}'.format(return_value)]
    return '\n'.join(lines)


def sqla_mixin_column(c, fkey=None, unique=False):
    return sqla_column(c, fkey, unique, True)


def odoo_column(c, ref_table=None):
    fname = get_target_name(c.name)
    attrs = []
    if ref_table:
        typ = 'Many2one'
        attrs.append(f"'{ref_table}'")
    else:
        typ = odoo_type(c)
    if not c.nullable:
        attrs.append('required=True')
    attrs_ = ', '.join(attrs)
    return f'{fname} = fields.{typ}({attrs_})'


CSV_COLS = ['Field', 'Type', 'Primary', 'Attribute', 'Unique', 'References']


def csv_column(c, fkey='', unique=''):
    fname = get_target_name(c.name)
    typ = sqla_type(c)
    if c.primary_key:
        primary = 'Y'
    else:
        primary = ''
    if c.nullable:
        attribute = ''
    else:
        attribute = 'NOT NULL'
    return [fname, typ, primary, attribute, unique, fkey]


def change_column_def(c):
    fieldname = get_target_name(c.name)
    orig_type = str(c.type).lower()
    if orig_type in BooleanTypes:
        return Column(
            Boolean, nullable=c.nullable, name=fieldname,
            primary_key=c.primary_key)
    if orig_type in FloatTypes:
        return Column(
            Float, nullable=c.nullable, name=fieldname,
            primary_key=c.primary_key)
    if orig_type in StringTypes or c.type.python_type in StringTypes:
        if orig_type == 'inet':
            typ = String(15)
        elif c.type.length:
            typ = String(c.type.length)
        else:
            typ = Text
        return Column(
            typ, nullable=c.nullable, name=fieldname,
            primary_key=c.primary_key)
    if c.type.python_type in IntTypes:
        return Column(
            c.type, nullable=c.nullable, name=fieldname,
            primary_key=c.primary_key)
    if c.type.python_type is float:
        return Column(
            Float, nullable=c.nullable, name=fieldname,
            primary_key=c.primary_key)
    if hasattr(c.server_default, 'arg') \
            and hasattr(c.server_default.arg, 'text') \
            and c.server_default.arg.text.strip().lower() == 'sysdate':
        return Column(
            DateTime, nullable=c.nullable, name=fieldname,
            primary_key=c.primary_key, default=func.now)
    if c.type.python_type is datetime:
        return Column(
            DateTime(timezone=c.type.timezone), nullable=c.nullable,
            name=fieldname, primary_key=c.primary_key)
    return Column(
        c.type, nullable=c.nullable, name=fieldname,
        primary_key=c.primary_key)


def get_primary_key(t):
    k = []
    for c in t.columns:
        if c.primary_key:
            k.append(c.name)
    return k


def has_primary_key(t):
    for c in t.columns:
        if c.primary_key:
            return True


def get_first_fields(t, field_count=10):
    fields = []
    i = 0
    for c in t.columns:
        i += 1
        fields.append(c.name)
        if i > field_count:
            break
    return fields


def change_foreign_key(constraint):
    references = []
    column_names = [column for column in constraint.columns]
    for e in constraint.elements:
        references.append(e.target_fullname)
    return sqlalchemy.schema.ForeignKeyConstraint(column_names, references)


def get_constraints(orm):
    constraints = []
    # Avoid "RuntimeError: Set changed size during iteration"
    const_list = list(orm.constraints)
    for c in const_list:
        if c.__class__ == sqlalchemy.schema.PrimaryKeyConstraint:
            continue
        if c.__class__ == sqlalchemy.schema.ForeignKeyConstraint:
            c = change_foreign_key(c)
            constraints.append(c)
        else:
            constraints.append(c)
    return constraints


def get_structure(orm):
    cols = []
    for c in orm.columns:
        c = change_column_def(c)
        cols.append(c)
    pkeys = get_primary_key(orm)
    constraints = get_constraints(orm)
    fkey_constraints = []
    for c in constraints:
        if c.__class__ == sqlalchemy.schema.ForeignKeyConstraint:
            fkeys = []
            for name in c.columns:
                name = get_target_name(name)
                fkeys.append(name)
            ref_fields = []
            for e in c.elements:
                t = e.target_fullname.split('.')
                ref_table = '.'.join(t[:-1])
                ref_field = t[-1]
                ref_fields.append(ref_field)
            schema_target = get_conf('schema_target')
            if schema_target:
                ref_table = '.'.join([schema_target, ref_table])
            fkey_constraint = (fkeys, ref_table, ref_fields)
            fkey_constraints.append(fkey_constraint)
    unique_constraints = []
    option = registry['option']
    try:
        no_unique_constraint = getattr(option, 'no_unique_constraint')
    except AttributeError:
        no_unique_constraint = False
    if not no_unique_constraint:
        db = registry['db']
        for constr in db.inspect.get_unique_constraints(
                orm.name, schema=orm.schema):
            unique_constraints.append(constr['column_names'])
    return orm.name, cols, pkeys, fkey_constraints, unique_constraints


def show_sql(tablename, lines):
    nice_lines = []
    for line in lines:
        s = '\n  ' + line
        nice_lines.append(s)
    s = 'CREATE TABLE {table} ({lines});'.format(
        table=tablename, lines=','.join(nice_lines))
    print(s)


def sql_extract_fkey_constraints(fkey_constraints):
    fkey_singles = dict()
    fkey_composites = []
    for fkeys, ref_table, ref_fields in fkey_constraints:
        if len(fkeys) == 1:
            fkey = fkeys[0]
            fkey_singles[fkey] = [ref_table, ref_fields[0]]
        else:
            fkeys_ = ', '.join(fkeys)
            fkey_composites.append([fkeys, ref_table, ref_fields])
    return fkey_singles, fkey_composites


def sqla_extract_fkey_constraints(fkey_constraints):
    fkey_singles = dict()
    fkey_composites = dict()
    for fkeys, ref_table, ref_fields in fkey_constraints:
        if len(fkeys) == 1:
            fkey = fkeys[0]
            ref = '.'.join([ref_table, ref_fields[0]])
            fkey_singles[fkey] = ref
        else:
            ref = []
            for ref_field in ref_fields:
                cls_ = get_class_name(ref_table)
                ref.append('{t}.{f}'.format(t=cls_, f=ref_field))
            fkeys_ = ', '.join(fkeys)
            fkey_composites[fkeys_] = ', '.join(ref)
    return fkey_singles, fkey_composites


def extract_unique_constraints(unique_constraints):
    singles = []
    composites = []
    for keys in unique_constraints:
        if keys[1:]:
            composites.append(keys)
        else:
            singles.append(keys[0])
    return singles, composites


def get_args_line(fkey_composites, unique_composites, orm=None):
    args = []
    for keys in unique_composites:
        keys_ = []
        for key in keys:
            keys_.append("'{k}'".format(k=str(key)))
        s = '\n        UniqueConstraint({keys})'.format(keys=', '.join(keys_))
        args.append(s)
    for fkeys in fkey_composites:
        ref = fkey_composites[fkeys]
        s = '\n        ForeignKeyConstraint([{fkeys}], [{ref}])'.format(
            fkeys=fkeys, ref=ref)
        args.append(s)
    if orm is not None and orm.schema:
        s = "\n        dict(schema='{s}')".format(s=orm.schema)
        args.append(s)
    if not args:
        return args
    if len(args) == 1:
        return args[0].strip()
    s = ','.join(args)
    return f'({s})'


def get_args_line_sqla(fkey_composites, unique_composites, orm=None):
    s = get_args_line(fkey_composites, unique_composites, orm)
    return s and f'__table_args__ = {s}'


def get_args_line_sqla_mixin(fkey_composites, unique_composites, orm=None):
    s = get_args_line(fkey_composites, unique_composites, orm)
    return s and str_declared('__table_args__', s)


def get_args_line_odoo(unique_composites, orm=None):
    args = []
    for keys in unique_composites:
        keys_ = []
        keys_name = []
        for key in keys:
            k = str(key)
            keys_.append(f"'{k}'")
            keys_name.append(k)
        keys_str = ', '.join(keys_name)
        msg = f'{keys_str} must be unique'
        name = '_'.join(keys_name)
        name = f'unique_{name}'
        s = ' ' * 8 + f"('{name}', 'UNIQUE({keys_str})', '{msg}')"
        args.append(s)
    if not args:
        return args
    s = ',\n'.join(args)
    return f'_sql_constraints = [\n{s}]'


def get_class_name(tablename):
    return tablename.title().replace('_', '')


def str_sqla_import():
    lines = ['from sqlalchemy import (']
    lines += [' ' * 4 + x + ',' for x in cache_sqla_import]
    lines += [' ' * 4 + ')']
    return '\n'.join(lines)


def show_sqla(tablename, lines):
    nice_lines = []
    for line in lines:
        s = '\n    ' + line
        nice_lines.append(s)
    cname = get_class_name(tablename)
    rows = [str_sqla_import()]
    rows.append('\n')
    s = 'class {c}(Base):{lines}'.format(
        c=cname, lines=''.join(nice_lines))
    rows.append(s)
    print('\n'.join(rows))


def show_sqla_mixin(tablename, lines):
    cname = get_class_name(tablename)
    rows = [str_sqla_import()]
    rows.append('from sqlalchemy.ext.declarative import declared_attr')
    rows.append('\n')
    s = 'class {c}Mixin:{lines}'.format(
        c=cname, lines='\n'.join(lines))
    rows.append(s)
    print('\n'.join(rows))


def str_odoo_import():
    lines = ['from odoo import (']
    lines += [' ' * 4 + x + ',' for x in ['models', 'fields']]
    lines += [' ' * 4 + ')']
    return '\n'.join(lines)


def show_odoo(tablename, lines):
    nice_lines = []
    for line in lines:
        s = '\n    ' + line
        nice_lines.append(s)
    cname = get_class_name(tablename)
    rows = [str_odoo_import()]
    rows.append('\n')
    lines_ = ''.join(nice_lines)
    s = f'class {cname}(models.Model):{lines_}'
    rows.append(s)
    print('\n'.join(rows))


def show_csv(lines):
    f = StringIO()
    c = csv.writer(f)
    c.writerow(CSV_COLS)
    for line in lines:
        c.writerow(line)
    print(f.getvalue())
    f.close()


class Info:
    """
    This is a class for generate table structure in various format.
    """

    def show_sql(self, Source):
        """
        The function to display a table structure in SQL.

        Parameters:
            Source (Table object): SQLAlchemy Table object

        Returns:
            SQL (str): SQL for CREATE TABLE
        """
        lines = []
        table_name, cols, pkeys, fkey_constraints, unique_constraints = \
            get_structure(Source)
        is_single_pkey = not pkeys[1:]
        fkey_singles, fkey_composites = \
            sql_extract_fkey_constraints(fkey_constraints)
        unique_singles, unique_composites = \
            extract_unique_constraints(unique_constraints)
        for c in cols:
            unique = c.name in unique_singles
            if c.name in fkey_singles:
                ref_table, ref_fields = fkey_singles[c.name]
                fkey = 'REFERENCES {ref_table} ({ref_fields})'.format(
                    ref_table=ref_table, ref_fields=ref_fields)
                field_type = sql_column(c, is_single_pkey, fkey, unique)
            else:
                field_type = sql_column(c, is_single_pkey, unique=unique)
            lines.append(field_type)
        if pkeys[1:]:  # multi ?
            pkey_line = 'PRIMARY KEY ({pkeys})'.format(pkeys=', '.join(pkeys))
            lines.append(pkey_line)
        for keys in unique_composites:
            s = 'UNIQUE ({keys})'.format(keys=', '.join(keys))
            lines.append(s)
        for fkey, ref_table, ref_fields in fkey_composites:
            s = 'FOREIGN KEY ({c}) REFERENCES {ref_table} ({ref_fields})'. \
                format(
                    c=', '.join(fkey), ref_table=ref_table,
                    ref_fields=', '.join(ref_fields))
            lines.append(s)
        if Source.schema:
            tablename = '.'.join([Source.schema, Source.name])
        else:
            tablename = Source.name
        show_sql(tablename, lines)

    def show_sqla(self, Source):
        """
        The function to display a table structure in Python language.

        Parameters:
            Source (Table object): SQLAlchemy Table object

        Returns:
            Table class (str): Python script
        """
        cache_sqla_import.clear()
        cache_sqla_import.append('Column')
        lines = ["__tablename__ = '{}'".format(Source.name)]
        table_name, cols, pkeys, fkey_constraints, unique_constraints = \
            get_structure(Source)
        fkey_singles, fkey_composites = \
            sqla_extract_fkey_constraints(fkey_constraints)
        unique_singles, unique_composites = \
            extract_unique_constraints(unique_constraints)
        for c in cols:
            unique = c.name in unique_singles
            if c.name in fkey_singles:
                ref = fkey_singles[c.name]
                fkey = "ForeignKey('{}')".format(ref)
                field_type = sqla_column(c, fkey, unique)
            else:
                field_type = sqla_column(c, unique=unique)
            lines.append(field_type)
        args_line = get_args_line_sqla(
            fkey_composites, unique_composites, Source)
        if args_line:
            lines.append(args_line)
        show_sqla(Source.name, lines)

    def show_sqla_mixin(self, Source):
        """
        The function to display a table structure in Python language without
        Base class as a parent.

        A table without Base class as the parent is intended to be the parent
        of the actual table class. That way the table schema can be determined
        later.

        Parameters:
            Source (Table object): SQLAlchemy Table object

        Returns:
            Table abstract class (str): Python script
        """
        cache_sqla_import.clear()
        cache_sqla_import.append('Column')
        line = str_declared('__tablename__', "'{}'".format(Source.name))
        lines = [line]
        table_name, cols, pkeys, fkey_constraints, unique_constraints = \
            get_structure(Source)
        fkey_singles, fkey_composites = \
            sqla_extract_fkey_constraints(fkey_constraints)
        unique_singles, unique_composites = \
            extract_unique_constraints(unique_constraints)
        for c in cols:
            unique = c.name in unique_singles
            if c.name in fkey_singles:
                ref = fkey_singles[c.name]
                fkey = f"ForeignKey('{ref}')"
                field_type = sqla_mixin_column(c, fkey, unique)
            else:
                field_type = sqla_mixin_column(c, unique=unique)
            lines.append(field_type)
        args_line = get_args_line_sqla_mixin(
            fkey_composites, unique_composites, Source)
        if args_line:
            lines.append(args_line)
        show_sqla_mixin(Source.name, lines)

    def show_odoo(self, Source):
        """
        The function to display a table structure in Odoo.

        Parameters:
            Source (Table object): SQLAlchemy Table object

        Returns:
            Table class (str): Python script
        """
        lines = [f"_name = '{Source.name}'"]
        table_name, cols, pkeys, fkey_constraints, unique_constraints = \
            get_structure(Source)
        fkey_singles, fkey_composites = \
            sqla_extract_fkey_constraints(fkey_constraints)
        for c in cols:
            if c.name in fkey_singles:
                ref = fkey_singles[c.name]
                ref_table = ref.split('.')[-2]
                field_type = odoo_column(c, ref_table)
            else:
                field_type = odoo_column(c)
            lines.append(field_type)
        if pkeys:
            unique_constraints.append(pkeys)
        args_line = get_args_line_odoo(unique_constraints, Source)
        if args_line:
            lines.append(args_line)
        show_odoo(Source.name, lines)

    def show_csv(self, Source):
        """
        The function to display a table structure in CSV format.

        Parameters:
            Source (Table object): SQLAlchemy Table object

        Returns:
            Table abstract class (str): Python script
        """
        lines = []
        table_name, cols, pkeys, fkey_constraints, unique_constraints = \
            get_structure(Source)
        fkey_singles, fkey_composites = \
            sqla_extract_fkey_constraints(fkey_constraints)
        unique_singles, unique_composites = \
            extract_unique_constraints(unique_constraints)
        for c in cols:
            unique = c.name in unique_singles and 'Y' or ''
            if c.name in fkey_singles:
                ref = fkey_singles[c.name]
                line = csv_column(c, ref, unique)
            else:
                line = csv_column(c, unique=unique)
            lines.append(line)
        show_csv(lines)

    def show(self, tablename, fmt='sql'):
        """
        The function to display a table structure in various format.

        Parameters:
            Table name (str): if the schema is mentioned separate by point

        Returns:
            Table structure (str)
        """
        schema, tablename = split_schema(tablename)
        db = registry['db']
        if schema:
            t = db.get_table(tablename, schema)
        else:
            schema_source = get_conf('schema_source') or None
            t = db.get_table(tablename, schema_source)
        option = registry['option']
        if option.sqla:
            self.show_sqla(t)
        elif option.sqla_mixin:
            self.show_sqla_mixin(t)
        elif option.odoo:
            self.show_odoo(t)
        elif option.csv:
            self.show_csv(t)
        else:
            self.show_sql(t)


def main(argv):
    help_conf = 'configuration file'
    pars = ArgumentParser()
    pars.add_argument('conf', help=help_conf)
    pars.add_argument('--table', required=True)
    pars.add_argument('--sqla', action='store_true')
    pars.add_argument('--sqla-mixin', action='store_true')
    pars.add_argument('--colander', action='store_true')
    pars.add_argument('--odoo', action='store_true')
    pars.add_argument('--csv', action='store_true')
    pars.add_argument('--no-unique-constraint', action='store_true')
    option = pars.parse_args(argv)
    conf_file = option.conf
    registry['conf'] = conf = RawConfigParser()
    conf.read(conf_file)
    registry['option'] = option
    registry['db'] = DBProfile(conf.get('main', 'db_url_source'))
    info = Info()
    tablenames = option.table.split(',')
    for tablename in tablenames:
        info.show(tablename)
        print('')


if __name__ == '__main__':
    main(sys.argv[1:])

import os
import sys
import csv
from argparse import ArgumentParser
from configparser import RawConfigParser
from sqlalchemy import (
    engine_from_config,
    text,
    )


help_conf = 'configuration file'

pars = ArgumentParser()
pars.add_argument('conf', help=help_conf)
pars.add_argument('--sql', help='query', required=True)
option = pars.parse_args(sys.argv[1:])

conf_file = option.conf
conf = RawConfigParser()
conf.read(conf_file)
conf = dict(conf.items('main'))

sql = option.sql
if os.path.exists(sql):
    sql = open(sql).read()
sql = sql.strip()

fieldnames = []
engine = engine_from_config(conf, 'source.')
with engine.connect() as conn:
    q = conn.execute(text(sql))
    if sql.upper().find('SELECT') < 0:
        print('Done.')
        sys.exit()
    for row in q:
        if not fieldnames:
            fieldnames = row._fields
            print(fieldnames)
        vals = []
        for fieldname in fieldnames:
            val = row._mapping[fieldname]
            vals.append(val)
        print(vals)

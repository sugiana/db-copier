import os
import sys
import csv
from argparse import ArgumentParser
from configparser import ConfigParser
from sqlalchemy import (
    engine_from_config,
    text,
    )
from tools import module_source


default_output = 'output.csv'
help_conf = 'configuration file'
help_output = 'default ' + default_output
pars = ArgumentParser()
pars.add_argument('conf', help=help_conf)
pars.add_argument('--sql', help='query')
pars.add_argument('--output', default=default_output, help=help_output)
option = pars.parse_args(sys.argv[1:])

conf_file = option.conf
sql = option.sql
if os.path.exists(sql):
    sql = open(sql).read()
sql = sql.strip()
filename = option.output

conf = ConfigParser()
conf.read(conf_file)
conf = dict(conf.items('main'))

fieldnames = []
engine = engine_from_config(conf, 'source.')
with engine.connect() as conn:
    q = conn.execute(text(sql))
    with open(filename, 'w') as f_obj:
        f = csv.writer(f_obj)
        for row in q:
            if not fieldnames:
                fieldnames = row._fields
                f.writerow(fieldnames)
            vals = []
            for fieldname in fieldnames:
                val = row._mapping[fieldname]
                vals.append(val)
            f.writerow(vals)
print(f'File {filename} saved')

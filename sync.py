import sys
from time import time
from sqlalchemy import select
from sqlalchemy.sql.expression import text
from tools import (
    plain_value,
    plain_values,
    is_same,
    split_schema,
    humanize_time,
    )
from models import DBProfile


class Daemon:
    def __init__(self, log):
        self.log = log

    def info(self, msg):
        self.log.info(msg)

    def error(self, msg):
        self.log.error(msg)

    def warning(self, msg):
        self.log.warning(msg)


def is_str(s):
    if sys.version_info.major > 2:
        return isinstance(s, str)
    return isinstance(s, str) or isinstance(s, unicode)


def get_update_fields(t):
    k = []
    for c in t.__table__.columns:
        if not c.primary_key:
            k.append(c.name)
    return k


def get_pkeys(t):
    k = []
    for c in t.__table__.columns:
        if c.primary_key:
            k.append(c.name)
    return k


class Synchronizer(Daemon):
    def __init__(
            self, log, db_url_source, db_url_target, sync_orm,
            source_table, target_table=None,
            pkeys=[], update_fields=[],
            source_has_pkey_constraint=False,
            target_has_pkey_constraint=False,
            target_pkeys=[]):
        Daemon.__init__(self, log)
        self.db_source = DBProfile(db_url_source)
        self.db_target = DBProfile(db_url_target)
        self.sync_orm = sync_orm
        self.source_has_pkey_constraint = source_has_pkey_constraint
        self.target_has_pkey_constraint = target_has_pkey_constraint
        self.source_orm = self.target_orm = None
        self.source_just_name = is_str(source_table)
        if self.source_just_name:
            self.source_schema, self.source_table_name = \
                split_schema(source_table)
        else:
            self.source_orm = source_table
            self.source_has_pkey_constraint = True
        if target_table:
            self.target_just_name = is_str(target_table)
            if self.target_just_name:
                self.target_schema, self.target_table_name = \
                    split_schema(target_table)
            else:
                self.target_orm = target_table
                self.target_has_pkey_constraint = True
        elif self.source_just_name:
            self.target_schema = self.source_schema
            self.target_table_name = self.source_table_name
        else:
            self.target_orm = self.source_orm
        if pkeys:
            self.pkeys = pkeys
        else:
            self.pkeys = get_pkeys(self.source_orm)
        self.target_pkeys = target_pkeys or self.pkeys
        if update_fields:
            self.update_fields = update_fields
        else:
            self.update_fields = get_update_fields(self.source_orm)
        if target_pkeys:
            self.insert_fields = target_pkeys + self.update_fields
        else:
            self.insert_fields = self.pkeys + self.update_fields
        self.prepare_for_orm()
        self.prepare_for_query()

    ##########
    # By ORM #
    ##########
    def prepare_for_orm(self):
        if self.source_has_pkey_constraint and not self.source_orm:
            self.source_orm = self.db_source.get_orm(
                    self.source_table_name, self.source_schema)
        if self.target_has_pkey_constraint and not self.target_orm:
            self.target_orm = self.db_target.get_orm(
                    self.target_table_name, self.target_schema)

    def get_query_by_orm(self, sync, orm, db, pkeys=[]):
        where = dict()
        if isinstance(sync, dict):
            pkey_values = sync
        else:
            pkey_values = sync.to_dict()
        pkeys_ = pkeys or self.pkeys
        for field in pkeys_:
            where[field] = pkey_values[field]
        return db.query(orm).filter_by(**where)

    def get_row_by_orm(self, sync, orm, db, pkeys=[]):
        q = self.get_query_by_orm(sync, orm, db, pkeys)
        return q.first()

    def get_source_by_orm(self, sync):
        return self.get_row_by_orm(sync, self.source_orm, self.db_source)

    def get_target_by_orm(self, sync):
        return self.get_row_by_orm(
                sync, self.target_orm, self.db_target, self.target_pkeys)

    def insert_orm(self, source_dict):
        d = dict()
        for fieldname in self.insert_fields:
            d[fieldname] = source_dict[fieldname]
        target = self.target_orm()
        target.from_dict(d)
        self.db_target.flush(target)

    def update_orm(self, source_dict, target_row):
        target_row.from_dict(source_dict)
        self.db_target.flush(target_row)

    def delete_orm(self, sync):
        where = dict()
        pkey_values = sync.to_dict()
        for field in self.pkeys:
            where[field] = pkey_values[field]
        q = self.db_target.query(self.target_orm).filter_by(**where)
        q.delete()
        self.db_target.flush()

    ############
    # By Query #
    ############
    def prepare_for_query(self):
        if not self.source_has_pkey_constraint:
            self.table_source = self.db_source.get_table(
                    self.source_table_name, self.source_schema)
        if not self.target_has_pkey_constraint:
            self.table_target = self.db_target.get_table(
                    self.target_table_name, self.target_schema)

    def get_row_by_query(self, sync, table, pkeys=[]):
        d = dict()
        if isinstance(sync, dict):
            pkey_values = sync
        else:
            pkey_values = sync.to_dict()
        sql = select([table])
        pkeys_ = pkeys or self.pkeys
        for field in pkeys_:
            f = getattr(table.c, field)
            v = pkey_values[field]
            sql = sql.where(f == v)
        q = table.metadata.bind.execute(sql)
        return q.fetchone()

    def get_source_by_query(self, sync):
        return self.get_row_by_query(sync, self.table_source)

    def get_target_by_query(self, sync):
        return self.get_row_by_query(
                sync, self.table_target, self.target_pkeys)

    def insert_by_query(self, source_dict):
        sql = self.table_target.insert().values(**source_dict)
        self.db_target.execute(sql)

    def update_by_query(self, update_dict, pkey_values):
        sql = self.table_target.update().values(**update_dict)
        self.update_or_delete_(sql, pkey_values)

    def delete_by_query(self, sync):
        sql = self.table_target.delete()
        self.update_or_delete_(sql, sync.to_dict())

    def update_or_delete_(self, sql, pkey_values):
        for field in self.pkeys:
            f = getattr(self.table_target.c, field)
            v = pkey_values[field]
            sql = sql.where(f == v)
        self.db_target.execute(sql)

    ################
    # Synchronizer #
    ################
    def run(self):
        count = self.db_source.get_count(self.sync_orm.__table__)
        if not count:
            print('Queue is empty, exit now.')
            return
        self.info('{c} queue found.'.format(c=count))
        begin_time = time()
        break_every_row = 10000
        row = 0
        self.before_loop()
        while True:
            row += 1
            if row > count:
                break
            sync = self.get_sync()
            if not sync:
                break
            msg, is_ok = self.sync_row(sync)
            self.after_sync_row(count, begin_time, row, sync, is_ok, msg)
            if row == break_every_row:
                break
        self.info('Done.')

    def before_loop(self):
        pass

    def get_id(self, sync):
        r = []
        for key in self.pkeys:
            value = getattr(sync, key)
            key_val = '{}:{}'.format(key, value)
            r.append(key_val)
        return ' '.join(r)

    def sync_all(self):
        order_by = text(', '.join(self.pkeys))
        q = self.db_source.query(self.source_orm).order_by(order_by)
        for row in q:
            t = self.sync_orm()
            for key in self.pkeys:
                value = getattr(row, key)
                setattr(t, key, value)
            self.db_source.flush(t)
        self.db_source.commit()

    def sync_row(self, sync):
        msg_id = self.get_id(sync)
        msg, ok = self._sync_row(sync)
        msg = ' '.join([msg_id, msg])
        return msg, ok

    def _sync_row(self, sync):
        source = self.get_source(sync)
        if source:
            return self.insert_or_update(sync, source)
        return self.source_not_found(sync, source)

    def source_not_found(self, sync, source):
        return 'source not found', True

    def insert_or_update(self, sync, source):
        target = self.get_target(sync)
        if target:
            return self.update(source.to_dict(), target)
        return self.insert(source.to_dict()), True

    def after_sync_row(self, count, begin_time, row, sync, is_ok, msg):
        self.delete_sync(sync)
        duration = time() - begin_time
        speed = duration / row
        remain_row = count - row
        finish_estimate = remain_row * speed
        estimate = humanize_time(finish_estimate)
        msg = '{row}/{count} {msg}, estimate {e}'.format(
                row=row, count=count, msg=msg, e=estimate)
        self.commit()
        if is_ok:
            self.info(msg)
        elif is_ok is None:
            self.warning(msg)
        else:
            self.error(msg)

    def get_source(self, sync):
        if self.source_has_pkey_constraint:
            return self.get_source_by_orm(sync)
        return self.get_source_by_query(sync)

    def get_target(self, sync):
        if self.target_has_pkey_constraint:
            return self.get_target_by_orm(sync)
        return self.get_target_by_query(sync)

    def insert(self, source_dict):
        if self.target_has_pkey_constraint:
            self.insert_orm(source_dict)
        else:
            self.insert_by_query(source_dict)
        return 'INSERT {d}'.format(d=plain_values(source_dict))  # log message

    def update(self, source_dict, target, update_fields=[]):
        if self.target_has_pkey_constraint:
            target_dict = target.to_dict()
        else:
            target_dict = dict(target)
        if not update_fields:
            update_fields = self.update_fields
        target_update = dict()
        log_msg = []
        for field in update_fields:
            if field not in source_dict:
                continue
            source_value = source_dict[field]
            target_value = target_dict[field]
            if not is_same(source_value, target_value):
                target_update[field] = source_value
                log_source_value = plain_value(source_value)
                log_target_value = plain_value(target_value)
                log_msg.append('{f} {t} to be {s}'.format(
                    f=field, t=[log_target_value], s=[log_source_value]))
        if target_update:
            if self.target_has_pkey_constraint:
                self.update_orm(target_update, target)
            else:
                self.update_by_query(target_update, source_dict)
            msg = 'UPDATE change {msg}'.format(msg=', '.join(log_msg))
            return msg, True
        for field in self.insert_fields:
            if field not in source_dict:
                continue
            source_value = source_dict[field]
            target_update[field] = plain_value(source_value)
        msg = 'already same {d}'.format(d=target_update)
        return msg, None

    def delete(self, sync):
        if self.target_has_pkey_constraint:
            self.delete_orm(sync)
        else:
            self.delete_by_query(sync)
        return 'DELETE {d}'.format(d=plain_values(row))

    def get_sync(self):
        q = self.db_source.query(self.sync_orm).order_by(self.sync_orm.id)
        return q.first()

    def delete_sync(self, sync):
        q = self.db_source.query(self.sync_orm).filter_by(id=sync.id)
        q.delete()
        self.db_source.flush()

    def commit(self):
        self.db_target.commit()
        self.db_source.commit()

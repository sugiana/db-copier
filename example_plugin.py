from logging import getLogger
from sqlalchemy import text


log = getLogger('plugin wp')


def before_save(parent, source, target):
    nama_kec = source.CPM_KECAMATAN_WP
    sql = text(
            "SELECT id FROM pad.tblkecamatan WHERE kecamatannm = :nama")
    q = parent.db_target.execute(sql, nama=nama_kec)
    kec = q.first()
    if kec:
        target.kecamatan_id = kec.id
        nama_kel = source.CPM_KELURAHAN_WP
        if nama_kel:
            sql = text(
                    "SELECT id FROM pad.tblkelurahan "
                    "WHERE kecamatan_id = :kec_id "
                    "AND kelurahannm = :nama")
            q = parent.db_target.execute(sql, kec_id=kec.id, nama=nama_kel)
            kel = q.first()
            if kel:
                target.kelurahan_id = kel.id
            else:
                msg = f'Kelurahan {nama_kel} tidak ditemukan '\
                        'di pad.tblkelurahan'
                log.warning(msg)
        else:
            log.warning('Kelurahan tidak diisi')
    else:
        msg = f'Kecamatan {nama_kec} tidak ditemukan di pad.tblkecamatan'
        log.warning(msg)


def get_sql():
    with open('example.sql') as f:
        return f.read()

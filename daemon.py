import sys
import os
import signal
import logging
from time import sleep
from subprocess import (
    Popen,
    PIPE,
    )


def is_live(pid):
    try:
        os.kill(pid, 0)
    except OSError:
        return
    for i in range(3):
        p1 = Popen(['ps', 'ax'], stdout=PIPE)
        regex = '^{}{} '.format(' '*i, pid)
        p2 = Popen(['grep', regex], stdin=p1.stdout, stdout=PIPE)
        p3 = Popen(['grep', '-v', 'grep'], stdin=p2.stdout, stdout=PIPE)
        result = p3.communicate()
        found = result[0]
        if found:
            return True


def read_pid_file(filename):
    try:
        f = open(filename)
        s = f.read()
        f.close()
        s = s.split()
        s = s[0]
        pid = int(s)
        return pid
    except IOError:
        return
    except ValueError:
        return
    except IndexError:
        return


def write_pid_file(filename):
    pid = os.getpid()
    f = open(filename, 'w')
    f.write(str(pid))
    f.close()
    return pid


def stop_daemon(pid_file):
    pid = read_pid_file(pid_file)
    if not pid:
        sys.exit()
    if not is_live(pid):
        sys.exit()
    print('kill {} by signal'.format(pid))
    os.kill(pid, signal.SIGTERM)
    i = 0
    while i < 5:
        sleep(1)
        i += 1
        if not is_live(pid):
            sys.exit()
    print('kill {p} by force'.format(p=pid))
    os.kill(pid, signal.SIGKILL)
    sys.exit()


log_format = '%(asctime)s %(levelname)s %(name)s %(message)s'
formatter = logging.Formatter(log_format)


def create_log(log_file):
    file_handler = logging.FileHandler(log_file)
    file_handler.setFormatter(formatter)
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(formatter)
    log = logging.getLogger(sys.argv[0])
    log.setLevel(logging.DEBUG)
    log.addHandler(file_handler)
    log.addHandler(console_handler)
    return log

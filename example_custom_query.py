from custom_query import Query as BaseQuery


class Query(BaseQuery):
    def __init__(self, db, nothing=None):
        super().__init__(db, 'patda_wp')

    def get_where(self, q):
        return q.where(self.table.c.CPM_NAMA_WP.ilike('%iwan%'))
